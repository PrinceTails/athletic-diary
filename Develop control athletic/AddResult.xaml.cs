﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.Data.SQLite;
using Microsoft.Win32;
using System.IO;
using System.Reflection;

namespace Develop_control_athletic
{
    /// <summary>
    /// Логика взаимодействия для AddResult.xaml
    /// </summary>
    public partial class AddResult : Window
    {
        private double weight, leftArm, rightArm, chest, stomach, leftHip, rightHip;
        string fileFront, fileSide;

        private void chooseSideButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                OpenFileDialog ofd = new OpenFileDialog();
                if (ofd.ShowDialog() == true)
                {
                    ImageSource asd = new BitmapImage(new Uri(ofd.FileName));
                    sidePhotoImage.Source = asd;
                    fileSide = ofd.FileName;
                }
            }
            catch (Exception msg)
            {
                MessageBox.Show("Выбранный файл не является изображением");
                return;
            }
        }

        private void chooseFrontButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                OpenFileDialog ofd = new OpenFileDialog();
                if (ofd.ShowDialog() == true)
                {
                    ImageSource asd = new BitmapImage(new Uri(ofd.FileName));
                    frontPhotoImage.Source = asd;
                    fileFront = ofd.FileName;
                }
            }
            catch(Exception msg)
            {
                MessageBox.Show("Выбранный файл не является изображением");
                return;
            }
        }

        private SQLiteConnection sql;
        private SQLiteDataAdapter sda;

        private void Image_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if(frontPhotoImage.Source == null)
            {

            }
        }

        public AddResult()
        {
            InitializeComponent();
            sql = new SQLiteConnection(App.connectionString);
            sql.Open();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (!double.TryParse(weightBox.Text, out weight))
            {
                MessageBox.Show("Не число! (Вес)");
                return;
            }
            if (!double.TryParse(rightArmBox.Text, out rightArm))
            {
                MessageBox.Show("Не число! (Правая рука)");
                return;
            }
            if (!double.TryParse(leftArmBox.Text, out leftArm))
            {
                MessageBox.Show("Не число! (Левая рука)");
                return;
            }
            if (!double.TryParse(chestBox.Text, out chest))
            {
                MessageBox.Show("Не число! (Грудь)");
                return;
            }
            if (!double.TryParse(stomachBox.Text, out stomach))
            {
                MessageBox.Show("Не число! (Живот)");
                return;
            }
            if (!double.TryParse(rightHipBox.Text, out rightHip))
            {
                MessageBox.Show("Не число! (Правое бедро)");
                return;
            }
            if (!double.TryParse(leftHipBox.Text, out leftHip))
            {
                MessageBox.Show("Не число! (Левое бедро)");
                return;
            }

            DateTime recordTime = DateTime.Now;
            string newPathFront = null;
            string newPathSide = null;
            string path = System.IO.Path.GetFullPath(Assembly.GetExecutingAssembly().Location);
            string filenameApp = System.IO.Path.GetFileName(Assembly.GetExecutingAssembly().Location);
            string directoryPath = path.Remove(path.Length - filenameApp.Length);

            if (frontPhotoImage.Source!=null)
            {
                newPathFront = @"Photos\front" + recordTime.GetHashCode();
                File.Copy(fileFront, directoryPath + newPathFront);
            }

            if(sidePhotoImage.Source!=null)
            {
                newPathSide = @"Photos\side" + recordTime.GetHashCode();
                File.Copy(fileSide, directoryPath + newPathSide);
            }
            
            
            SQLiteCommand cmd = new SQLiteCommand("INSERT INTO [Records] VALUES (@d, @w, @ra, @la, @c, @s,@rh,@lh,@f,@side)", sql);
            cmd.Parameters.Add("d", DbType.DateTime);
            cmd.Parameters.Add("w", DbType.Double);
            cmd.Parameters.Add("ra", DbType.Double);
            cmd.Parameters.Add("la", DbType.Double);
            cmd.Parameters.Add("c", DbType.Double);
            cmd.Parameters.Add("s", DbType.Double);
            cmd.Parameters.Add("rh", DbType.Double);
            cmd.Parameters.Add("lh", DbType.Double);
            cmd.Parameters.Add("f", DbType.String);
            cmd.Parameters.Add("side", DbType.String);

            cmd.Parameters["d"].Value = recordTime;
            cmd.Parameters["w"].Value = weight;
            cmd.Parameters["ra"].Value = rightArm;
            cmd.Parameters["la"].Value = leftArm;
            cmd.Parameters["c"].Value = chest;
            cmd.Parameters["s"].Value = stomach;
            cmd.Parameters["rh"].Value = rightHip;
            cmd.Parameters["lh"].Value = leftHip;
            cmd.Parameters["f"].Value = newPathFront;
            cmd.Parameters["side"].Value =newPathSide;

            cmd.ExecuteNonQuery();

            this.DialogResult = true;
        }

        private void isDigit(object sender, KeyEventArgs e)
        {
            //if (!char.IsDigit(e.Key))
            //{
            //    e.Handled = true;
            //}
            if (e.Key == Key.Back)
            {
                e.Handled = false;
            }
            if (e.Key == Key.Delete)
            {
                e.Handled = false;
            }
            
        }
    }
}
