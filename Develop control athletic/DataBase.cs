﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SQLite;

namespace Athletic_Diary
{
    class DataBase
    {
        public string fileName;
        SQLiteConnection sql_connection;
        SQLiteCommand sql_command;
        public static string connectionString;

        public DataBase()
        {
            sql_connection = new SQLiteConnection();
            sql_command = new SQLiteCommand();
            fileName = "database.db";
            connectionString = "Data Source=" + fileName + ";Version=3;";
        }

        public DataBase(string fn)
        {
            sql_connection = new SQLiteConnection();
            sql_command = new SQLiteCommand();
            fileName = fn;
            connectionString = "Data Source=" + fileName + ";Version=3;";

        }

        public static DataBase CreateBase(string fn)
        {
            DataBase db = new DataBase(fn);
            SQLiteConnection.CreateFile(db.fileName);
            return db;
        }

        public bool DefautFillBase()
        {
            sql_connection = new SQLiteConnection(connectionString);
            sql_connection.Open();
            sql_command.Connection = sql_connection;

            sql_command.CommandText = @"CREATE TABLE Records(
                                         Дата DATETIME   PRIMARY KEY
 
                                                               UNIQUE
                                                               NOT NULL,
                                         Вес DOUBLE NOT NULL,
                                         [Рука (П)]  DOUBLE NOT NULL,
                                         [Рука (Л)]   DOUBLE NOT NULL,
                                         Грудь DOUBLE NOT NULL,
                                         Живот DOUBLE NOT NULL,
                                         [Бедро (П)] DOUBLE NOT NULL,
                                         [Бедро (Л)]  DOUBLE NOT NULL,
                                         Анфас STRING,
                                         Профиль STRING
                                     );";
            if (sql_command.ExecuteNonQuery() != 0)
                return false;
            sql_connection.Close();
            return true;
        }

    }
}
