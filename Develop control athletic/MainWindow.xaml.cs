﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Reflection;

using System.Data.SQLite;
using System.Data;
using System.IO;
using Athletic_Diary;
using System.Windows.Controls.Primitives;

namespace Develop_control_athletic
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private SQLiteConnection sql;
        private SQLiteDataAdapter sda;
        static string fileName = "database.db";
        string appPath;
        public MainWindow()
        {
            InitializeComponent();
            appPath = applicationPath();
            if (!File.Exists(fileName))
            {
                DataBase db = DataBase.CreateBase(fileName);
                if (!db.DefautFillBase())
                {
                    MessageBox.Show("Не удалось создать базу данных");
                    File.Delete(fileName);
                    this.Close();
                }
                sql = new SQLiteConnection(App.connectionString);
                sql.Open();
            }
            else
            {
                sql = new SQLiteConnection(App.connectionString);
                sql.Open();
                sda = new SQLiteDataAdapter("SELECT * FROM [Records]", App.connectionString);
                SQLiteCommandBuilder scb = new SQLiteCommandBuilder(sda);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                dt.Columns.Remove(dt.Columns[9]);
                dt.Columns.Remove(dt.Columns[8]);
                dt.Columns[0].ReadOnly = true;
                resultsTable.ItemsSource = dt.DefaultView;
            }
            
        }

        private void addResult_Click(object sender, RoutedEventArgs e)
        {
            AddResult res = new AddResult();
            if (res.ShowDialog() == true)
            {
                sda = new SQLiteDataAdapter("SELECT * FROM [Records]", App.connectionString);
                SQLiteCommandBuilder scb = new SQLiteCommandBuilder(sda);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                dt.Columns.Remove(dt.Columns[9]);
                dt.Columns.Remove(dt.Columns[8]);
                dt.Columns[0].ReadOnly = true;
                resultsTable.ItemsSource = dt.DefaultView;
                recordResult.UpdateLayout();
            }
        }

        private void resultsTable_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(resultsTable.SelectedItems.Count>1)
            {
                compare.IsEnabled = true;
            }
            else
            {
                compare.IsEnabled = false;
            }
        }
        string applicationPath()
        {
            string path = System.IO.Path.GetFullPath(Assembly.GetExecutingAssembly().Location);
            string filenameApp = System.IO.Path.GetFileName(Assembly.GetExecutingAssembly().Location);
            return path.Remove(path.Length - filenameApp.Length);
        }

        private void showPhotosButton_Click(object sender, RoutedEventArgs e)
        {
            if(resultsTable.SelectedItems.Count==1)
            {
                TextBlock date = resultsTable.Columns[0].GetCellContent(resultsTable.Items[resultsTable.SelectedIndex]) as TextBlock;
                ShowPhoto sp = new ShowPhoto(date.Text);
                sp.ShowDialog();

            }
            else
            {
                MessageBox.Show("Выберите ОДНУ строку.");
                return;
            }
        }
    }
}
