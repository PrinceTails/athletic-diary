﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using System.Data.SQLite;
using System.Data;
using System.Reflection;
using Develop_control_athletic;

namespace Athletic_Diary
{
    /// <summary>
    /// Логика взаимодействия для ShowPhoto.xaml
    /// </summary>
    public partial class ShowPhoto : Window
    {
        BitmapImage front, side;
        string appPath;
        private SQLiteConnection sql;

        public ShowPhoto(string date)
        {
            InitializeComponent();
            appPath = applicationPath();
            sql = new SQLiteConnection(App.connectionString);
            sql.Open();

            SQLiteCommand cmd = new SQLiteCommand("SELECT [Анфас],[Профиль] FROM [Records] WHERE [Дата] LIKE @d", sql);
            cmd.Parameters.Add("d", DbType.DateTime);
            DateTime dcm = DateTime.Parse(date);

            cmd.Parameters["d"].Value = dcm;
            using (SQLiteDataReader dr = cmd.ExecuteReader())
            {
                string ph1, ph2;
                dr.Read();
                ph1 = dr.GetString(0);
                ph2 = dr.GetString(1);
            }
        }

        string applicationPath()
        {
            string path = System.IO.Path.GetFullPath(Assembly.GetExecutingAssembly().Location);
            string filenameApp = System.IO.Path.GetFileName(Assembly.GetExecutingAssembly().Location);
            return path.Remove(path.Length - filenameApp.Length);
        }
    }
}
